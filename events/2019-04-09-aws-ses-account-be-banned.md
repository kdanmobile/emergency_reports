# 事件：AWS SES 帳號被停用
## 發生時間：2020-04-09 ~ 2020-04-13
## 錯誤內容

* AWS 警告 Complaint Rate 與 Bounce Rate 超出安全值

## 影響範圍

* 全系統無法寄信

## 短期處置

* 關閉有被攻擊疑慮的 api
  * https://www.kdanmobile.com/en/contact
  * mail share link to frientds
* nginx 阻斷相同 IP 的連續請求


## 長期處置

* bounce 機制轉移到 mail center 統一處理
* data center, anizone 的信，透過 mail center
* 舊 member center 的信，透過 mail center
* 研究並建立 mail center 的控制機制

## Timeline

* 4/9 AM 1:44 收到 AWS SES Complaint Review Period
* 4/9 AM 5:26 收到 AWS SES Bounce Review Period
* 4/9 PM 3:51 與行銷與Data Team 討論
  * 認為是一次性活動撈取名單條件有漏失造成，修正名單撈取條件
 * 事實證明還有其他因素，但當下未察覺
* 4/10 PM 2:57 收到 AWS SES Complaint Sending Pause
  * 透過 CloudWatch 確認 4/8 - 4/9 有大量信件寄出
  * 確認 cr_system 4/8 - 4/9 會員註冊人數與發信數量，無異常
  * 確認 sendy 4/8 - 4/9 campaigns ，無異常 無法取得更細節資訊確認問題發生點
  * 陸續請教昕奇科技，SES Review 與 AWS Support
* 4/11 AM 3:20 收到 AWS Support 回覆 
  * 透過 AWS Support 確認：大量信件由 noreply@system-kdanmobile.com 發出
  * 確認 4/8 - 4/9 相關系統變更，無異常 確認 4/8 - 4/9 相關 API 呼叫紀錄，無異常
  * 初步排除此問題為系統造成的可能性 懷疑為 SES Credential 外洩造成
* 4/11 AM 11:00 更新所有 SES Credential
* 4/11 AM 12:00 部署新的 SES Credential 到正式環境
* 4/11 PM 04:00 回覆 AWS Support 已經更新 Credential
* 4/11 PM 06:14 收到 SES Review 提供的 complaint sample mail
  * 確認信件來自 Kdan Cloud 的 Share Link 寄信功能
  * 檢查 Log backuped on S3
  * 發現確實有大量請求 (Kibana 由於不明原因未收錄到)
* 4/11 PM 07:51 將發生問題的功能停掉，並部署到正式環境
* 4/11 PM 08:03 回覆 SES Review 說明已經解決問題
* 4/12 AM 02:27 SES Review 來信
  * 提及 https://www.kdanmobile.com/en/contact 也有安全風險
* 4/12 PM 11:50 暫時將引起問題的功能移除並部署到正式環境
* 4/13 AM 12:43 回覆 SES Review 說明已經解決問題
* 4/13 PM 02:40 SES 服務回復正常
