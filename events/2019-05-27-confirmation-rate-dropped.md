# 事件：驗證率突然下滑
## 發生時間：2020-05-20 ~ 2020-05-27
## 錯誤內容

* kdan-cloud 接收到許多迷失的驗證請求 (應該導至 member_center)


## 影響範圍

* 驗證率由 25% 降至 12%

## 短期處置

* 暫時關閉 Review 信件
* nginx 轉址有部分入口未更改到的關係
  * cloud.kdanmobile.com --> member-center.kdanmobile.com
* 針對有進行驗證的用戶進行手動驗證

## 長期處置
* Review 信件改由其他方式寄送


## Timeline
* 5/11 驗證率 25%, bounce rate: 4.36%
* 5/12 驗證率 12%, bounce rate: 4.43%
* 5/20 收到 Data Team 通知，查無異常需再觀察
* 5/22 Review 信件，更改寄件者
  * kdanmobile@kdanmobile.com 變更為 noreply@system-kdanmobile.com
* 5/23 驗證率 13%, bounce rate: 4.76
* 5/24 驗證率 26%, bounce rate: 4.16
* 5/25 暫時關閉 Review 信件
* 5/27 修正備份資料庫的問題，驗證率報表更新到最新數據
  * kdan-cloud 接收到許多迷失的驗證請求
* 5/27 針對迷失的驗證請求進行手動驗證(約 1000名)
