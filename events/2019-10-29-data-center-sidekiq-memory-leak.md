## 緣起 (Oct, 2019 updated)  
  近期服務不穩定而導致用戶體驗下降並影響業務推廣，歸納事件原因如下：

  * Data Center Sidekiq Memory Leak，導致檔案無法上傳
  * 機器遷移時疏忽，導致檔案無法上傳
  * 發大量 Push 時，不時會出現停滯，導致用戶延遲收到
  * AWS Route 53 遭受攻擊，導致檔案無法下載

## 短期處理
  * Data Center: 監控 Memory 並發警告到 mattermost，工程師手動重啟
  * 機器遷移: 工程師自行注意
  * Push: 發現停滯時，通知工程師手動排除
  * AWS 遭受攻擊: 詢問伊雲谷顧問建議發客服信與原廠洽詢賠償事宜 (需行銷夥伴協助)

## 長期處理
  * Data Center: 改以 Lambda 處理檔案，可同時解決 memory leak 與流量尖峰的問題 (處理中)
  * 機器遷移: 於導入 iso 時建立機器遷移的 SOP (待處理)
  * Push: 新增後台按鈕，讓發現問題的夥伴手動排除 (待處理)
  * AWS 遭受攻擊: 需以混合雲模式避免，研究 GCP 是否可以當作備援

## 監視與告警
  * 新增 CloudWatch 設定監控服務的 Memery 與 Disk Space
  * 研究 AWS Solution 如何更好的做事件預警
