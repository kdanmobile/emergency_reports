# 事件：kdanmobile.com SSL 根憑證過期
## 發生時間：2020-05-31 ~ 2020-06-01
## 錯誤內容：

* 從 socket_center 發出的 API  顯示 SSL Invalid
* 從 lambda 發出的 API 顯示 SSL Invalid

## 影響範圍：

* 變更個人/組織頭像，會失敗
* 有附件的任務，會失敗
* 建立任務：送出後，會顯示失敗但實際有寄送
* 簽署任務：送出後，會顯示失敗但實際有寄送

## 短期處置

* 從 Browser 觀察顯示正常
* 透過 SSL Checker 發現根憑證過期
* Namecheap 官網發現 Sectigo Root Certificate expiring May 30, 2020 公告
* 進行 Reissue
* 為各服務安裝新憑證

## 長期處置

* 在 Browser 上面檢視無法得知根憑證資訊
* 需改以 SSL Checker 確認，並提交 MIS 管制
* 經查 Namecheap 並未對 dev@kdanmobile.com 發出類似預警
* 將提出客訴請 Namecheap 改進
